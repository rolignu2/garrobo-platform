
<?php if(!$enabled): ?>
    <link href="<?php echo site_url();?>content/system/core/css/global/bootstrap/bootstrap.min.css" rel="stylesheet" id="zCoI6E4u" type="text/css"/>
<?php endif; ?>


<link href="<?php echo site_url();?>content/assets/global/css/error.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url();?>content/system/core/css/global/font_awesome/font-awesome.min.css" rel="stylesheet" id="6WNaMBDh" type="text/css"/>
<link href="<?php echo site_url();?>content/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" id="gB2CeKoz" type="text/css"/>
<link href="<?php echo site_url();?>content/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" id="Z4SIXf1Y" type="text/css"/>
<link href="<?php echo site_url();?>content/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" id="7SZCs2Pd" type="text/css"/>
<link href="<?php echo site_url();?>content/assets/global/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" id="lhPJvfGL" type="text/css"/>
<link href="<?php echo site_url();?>content/assets/global/css/components.css" rel="stylesheet" id="ToLWp6kA" type="text/css"/>
<link href="<?php echo site_url();?>content/assets/layouts/layout/css/layout.css" rel="stylesheet" id="az0Dgie4" type="text/css"/>



<div style="padding-top:10%" class="row">
    <div class="col-md-12 page-404">
        <div class="number font-red"> 404 </div>
        <div class="details">
            <h3>Oops! Estas perdido </h3>
            <p> No pudimos encontrar lo que buscas :(
                <br/>
            <form action="#">
                <div class="input-group input-medium">
                    <input type="text" class="form-control" placeholder="palabra clave">
                            <span class="input-group-btn">
                                <button type="button" class="btn red">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
            </form>
        </div>
    </div>
</div>

