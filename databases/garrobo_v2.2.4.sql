-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.9 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para garrobo
CREATE DATABASE IF NOT EXISTS `garrobo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `garrobo`;


-- Volcando estructura para tabla garrobo.ga_metadata
CREATE TABLE IF NOT EXISTS `ga_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `value` text,
  `id_user` int(10) unsigned DEFAULT '0',
  `id_rol` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='metadata es una tabla en la cual sus valores no son estaticos , eso quiere decir que la tabla\r\nno es uso especifico asi que solo tiene unos pares de campos y la mayoria se trabajara\r\ncon variables o con cadenas cuyo patrones estan definidos aplica con JSON y XML';

-- Volcando datos para la tabla garrobo.ga_metadata: 2 rows
DELETE FROM `ga_metadata`;
/*!40000 ALTER TABLE `ga_metadata` DISABLE KEYS */;
INSERT INTO `ga_metadata` (`id`, `key`, `value`, `id_user`, `id_rol`) VALUES
	(1, 'a', 'es prueba', 0, 0),
	(2, 'b', 'como estas', 0, 0);
/*!40000 ALTER TABLE `ga_metadata` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_nav
CREATE TABLE IF NOT EXISTS `ga_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `route` varchar(50) DEFAULT NULL,
  `objects` text,
  `components` text,
  `parent` varchar(255) DEFAULT NULL,
  `origins` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `privs` varchar(255) DEFAULT '0',
  `token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='navbar version 1.0 ';

-- Volcando datos para la tabla garrobo.ga_nav: 8 rows
DELETE FROM `ga_nav`;
/*!40000 ALTER TABLE `ga_nav` DISABLE KEYS */;
INSERT INTO `ga_nav` (`id`, `type`, `name`, `location`, `route`, `objects`, `components`, `parent`, `origins`, `active`, `privs`, `token`) VALUES
	(1, 'namespace', 'Menu', NULL, NULL, '{\n  \n  "icon" : "",\n  "redirect": "",\n  "target" : ""\n  \n}', '{}', '0', 'system', 1, '0', NULL),
	(2, 'sidebar', 'Home', '', NULL, '{\r\n  \r\n  "icon" : "fa fa-home",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '1', 'system', 1, '0', NULL),
	(3, 'section', 'Menu', '', NULL, '{\r\n  \r\n  "icon" : "",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '1', 'system', 0, '0', NULL),
	(4, 'namespace', 'Administrador', NULL, NULL, '{\r\n  \r\n  "icon" : "",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '0', 'system', 1, '1', NULL),
	(5, 'section', 'Usuarios', NULL, NULL, '{\r\n  \r\n  "icon" : "fa fa-users",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '4', 'system', 1, '1', NULL),
	(6, 'sidebar', 'Permisos', 'system=permission', 'permission', '{\r\n  \r\n  "icon" : "fa fa-terminal",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '5', 'system', 1, '1', NULL),
	(8, 'sub_menu', 'Mi perfil ', 'system=profile', 'profile', '{\r\n  \r\n  "icon" : "icon-user",\r\n  "redirect": "",\r\n  "target" : "",\r\n "place" : "1",\r\n "divider" : "true"\r\n  \r\n}', '{}', '0', 'system', 1, '1', NULL),
	(9, 'sub_menu', 'Cerrar Sesión', 'system=end_session', 'end_session', '{\r\n  \r\n  "icon" : "icon-key",\r\n  "redirect": "",\r\n  "target" : "",\r\n  "place" : "1"\r\n  \r\n}', '{}', '0', 'system', 1, '1', NULL);
/*!40000 ALTER TABLE `ga_nav` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_rols
CREATE TABLE IF NOT EXISTS `ga_rols` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `meta` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='la tabla de rols es la mas sencilla solo especificamos un nombre al rol \r\ntalves algun meta agregado a ello pero nada mas ';

-- Volcando datos para la tabla garrobo.ga_rols: 3 rows
DELETE FROM `ga_rols`;
/*!40000 ALTER TABLE `ga_rols` DISABLE KEYS */;
INSERT INTO `ga_rols` (`id`, `name`, `meta`) VALUES
	(1, 'administrator', NULL),
	(2, 'guess', NULL),
	(3, 'user', NULL);
/*!40000 ALTER TABLE `ga_rols` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_user
CREATE TABLE IF NOT EXISTS `ga_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` text NOT NULL,
  `data` text,
  `privileges` text,
  `last_connect` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(2) DEFAULT NULL,
  `connected` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Garrobo CMS base de datos del usuario , se definieron estas bases de datos \r\nde una forma en la que la experciendia sea ,ucho mejor. en palabras mas \r\ncristianas la base de datos debe de responder de forma rapida.';

-- Volcando datos para la tabla garrobo.ga_user: ~0 rows (aproximadamente)
DELETE FROM `ga_user`;
/*!40000 ALTER TABLE `ga_user` DISABLE KEYS */;
INSERT INTO `ga_user` (`id`, `username`, `email`, `password`, `data`, `privileges`, `last_connect`, `active`, `connected`) VALUES
	(1, 'admin', 'rolignu90@gmail.com', '7d7ff17eb8d7eb50d4e04c7ff096b291c0cdfa64a5d33d2ac5ef6fb253062d8ef29d7e82305db6df84f3f128bd415e9552131cfe488251c21e43c0aecf990fe0hBHnjDJeHG9ClaLf97Vc0KRyt/mC', '{\r\n  \r\n  "details" : \r\n  {\r\n     "name" : "Rolando Antonio",\r\n     "last_name" : "Arriaza Marroquin",\r\n     "register" : "2016-06-19",\r\n     "avatar" : "",\r\n     "occupation": "Ingeniero en sistemas",\r\n     "location": "El salvador",\r\n    "website" : "www.rolandoarriaza.com"\r\n  },\r\n  \r\n  "last_passwords": {}\r\n  \r\n}', '{\n\n    "parent" : "1",\n    "childs" : "2,3"\n  \n}', '2016-07-01 02:56:10', 1, 1);
/*!40000 ALTER TABLE `ga_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
