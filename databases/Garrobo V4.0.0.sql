-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         10.0.25-MariaDB-0ubuntu0.16.04.1 - Ubuntu 16.04
-- SO del servidor:              debian-linux-gnu
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para garrobo
CREATE DATABASE IF NOT EXISTS `garrobo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `garrobo`;


-- Volcando estructura para tabla garrobo.ga_metadata
CREATE TABLE IF NOT EXISTS `ga_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `value` longtext,
  `id_user` int(10) unsigned DEFAULT '0',
  `id_rol` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COMMENT='metadata es una tabla en la cual sus valores no son estaticos , eso quiere decir que la tabla\r\nno es uso especifico asi que solo tiene unos pares de campos y la mayoria se trabajara\r\ncon variables o con cadenas cuyo patrones estan definidos aplica con JSON y XML';

-- Volcando datos para la tabla garrobo.ga_metadata: 4 rows
/*!40000 ALTER TABLE `ga_metadata` DISABLE KEYS */;
INSERT INTO `ga_metadata` (`id`, `key`, `value`, `id_user`, `id_rol`) VALUES
	(25, 'user_lang', 'en', 2, 0),
	(26, 'log_file', '[{"id":"054aea9c08be0f041fb7fce0a099ca48","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 15:40:20 -0600","date":"Sun, 18 Sep 2016 15:40:20 -0600"},{"id":"77effe0df6b612a816833a3134779bc5","data":"[End Session][SYSTEM] No ahora porfavor  has cerrado sesion la fecha de Sun, 18 Sep 16 15:41:06 -0600","date":"Sun, 18 Sep 2016 15:41:06 -0600"},{"id":"38cf54d6639904ca262395f681bdfe93","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 15:41:13 -0600","date":"Sun, 18 Sep 2016 15:41:13 -0600"},{"id":"d17ecb5e138a198f396d505ec79e29e4","data":"[Avatar ][SYSTEM] No ahora porfavor  Avatar has been change in  Sun, 18 Sep 16 15:44:07 -0600","date":"Sun, 18 Sep 2016 15:44:07 -0600"},{"id":"24381b01011f143a72e8d522083d5585","data":"[End Session][SYSTEM] No ahora porfavor  has cerrado sesion la fecha de Sun, 18 Sep 16 16:48:23 -0600","date":"Sun, 18 Sep 2016 16:48:23 -0600"},{"id":"cb4579e83abe9ab88add0ad0d8ac99fd","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 16:54:25 -0600","date":"Sun, 18 Sep 2016 16:54:25 -0600"},{"id":"1e2324e93f07d4af8c8e04fe4958b271","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 19:37:43 -0600","date":"Sun, 18 Sep 2016 19:37:43 -0600"},{"id":"3f9ecbe0ffce655f275d9fc988a29c7e","data":"[End Session][SYSTEM] No ahora porfavor  has cerrado sesion la fecha de Sun, 18 Sep 16 19:38:13 -0600","date":"Sun, 18 Sep 2016 19:38:13 -0600"},{"id":"2638bddbd754f2989c5f6456dc09cfd8","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 19:38:18 -0600","date":"Sun, 18 Sep 2016 19:38:18 -0600"},{"id":"f06c725203aa6b08407398fff813dbcc","data":"[End Session][SYSTEM] No ahora porfavor  has cerrado sesion la fecha de Sun, 18 Sep 16 19:39:45 -0600","date":"Sun, 18 Sep 2016 19:39:45 -0600"}]', 2, 0),
	(23, 'user_lang', 'en', 1, 0),
	(24, 'log_file', '[{"id":"5e87f4cdcb7f51adb13aa6b76a9655f4","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 17:58:22 -0600","date":"Sun, 11 Sep 2016 17:58:22 -0600"},{"id":"07a63829cde30e58fdfa60240b51356d","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:01:04 -0600","date":"Sun, 11 Sep 2016 18:01:04 -0600"},{"id":"402e28c767bd7a5a1ba37cd4d5017a16","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:21:07 -0600","date":"Sun, 11 Sep 2016 18:21:07 -0600"},{"id":"5390926650136374f4b2dbdba6a1e094","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:23:30 -0600","date":"Sun, 11 Sep 2016 18:23:30 -0600"},{"id":"f88a79cd87d8edbfa31f1ed4b72e7fca","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:23:31 -0600","date":"Sun, 11 Sep 2016 18:23:31 -0600"},{"id":"eff80da8359f7a2777fb6fa9947f36a1","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:23:54 -0600","date":"Sun, 11 Sep 2016 18:23:54 -0600"},{"id":"b69206b2fc897289cdb01cf1350ce1e9","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:24:50 -0600","date":"Sun, 11 Sep 2016 18:24:50 -0600"},{"id":"871b80431008e9425c9466fa9e66bbba","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:25:25 -0600","date":"Sun, 11 Sep 2016 18:25:25 -0600"},{"id":"3d24a37e8452c2581f0899bf8a9518b2","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:28:23 -0600","date":"Sun, 11 Sep 2016 18:28:23 -0600"},{"id":"b2c4b503dd189c280a7745a9bf5a43c9","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:29:54 -0600","date":"Sun, 11 Sep 2016 18:29:54 -0600"},{"id":"29ec146e3c3935c1a92a2d49cb79c18d","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:31:00 -0600","date":"Sun, 11 Sep 2016 18:31:00 -0600"},{"id":"5c5842af755b2de5dfc86114bbabda86","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:31:53 -0600","date":"Sun, 11 Sep 2016 18:31:53 -0600"},{"id":"9ff5087644fa4cfd2c1b27b401e408b0","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:33:34 -0600","date":"Sun, 11 Sep 2016 18:33:34 -0600"},{"id":"9a53e78ac9c1ac318ba008094e11449e","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:37:43 -0600","date":"Sun, 11 Sep 2016 18:37:43 -0600"},{"id":"c57082777c3c3ea5ea145954d5eaaad8","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 18:39:06 -0600","date":"Sun, 11 Sep 2016 18:39:06 -0600"},{"id":"839b17098f142f369ffce63ed8346fd0","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 20:08:11 -0600","date":"Sun, 11 Sep 2016 20:08:11 -0600"},{"id":"ce631a89e525bce27aa1ac57c7eb5201","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 20:16:32 -0600","date":"Sun, 11 Sep 2016 20:16:32 -0600"},{"id":"3e9302fef636fe6a531e4359a7673d82","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 20:16:40 -0600","date":"Sun, 11 Sep 2016 20:16:40 -0600"},{"id":"e65f90284b83504277f5d97598b4ca79","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 20:16:46 -0600","date":"Sun, 11 Sep 2016 20:16:46 -0600"},{"id":"d662ffbb917bc120a43d19a61b7219fb","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 20:18:18 -0600","date":"Sun, 11 Sep 2016 20:18:18 -0600"},{"id":"ebfe87d0c2ccdccec04989c61d951588","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 20:19:48 -0600","date":"Sun, 11 Sep 2016 20:19:48 -0600"},{"id":"0e7231ea15f140b534ccfc9c58382380","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 20:21:17 -0600","date":"Sun, 11 Sep 2016 20:21:17 -0600"},{"id":"e2b5eb63a85dd7733c179c5e73aa6094","data":"[START SESSION][SYSTEM]  Start Session on Sun, 11 Sep 16 20:30:15 -0600","date":"Sun, 11 Sep 2016 20:30:15 -0600"},{"id":"e39fcd4de329f779261066acc3fcd481","data":"[START SESSION][SYSTEM]  Start Session on Mon, 12 Sep 16 19:14:40 -0600","date":"Mon, 12 Sep 2016 19:14:40 -0600"},{"id":"7e1d2e89643792405ff20adcbd257c39","data":"[START SESSION][SYSTEM]  Start Session on Thu, 15 Sep 16 09:08:17 -0600","date":"Thu, 15 Sep 2016 09:08:17 -0600"},{"id":"d6a3dd848e6f5e1ce8e1edea841c7e49","data":"[Avatar ][SYSTEM] Rolando Antonio Avatar has been change in  Thu, 15 Sep 16 11:44:38 -0600","date":"Thu, 15 Sep 2016 11:44:38 -0600"},{"id":"73b069438aaa2d83886c49eacb3aac66","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Thu, 15 Sep 16 12:09:32 -0600","date":"Thu, 15 Sep 2016 12:09:32 -0600"},{"id":"fbfcc92daa045a5b1c8ebaf39256e4ea","data":"[START SESSION][SYSTEM]  Start Session on Thu, 15 Sep 16 12:09:50 -0600","date":"Thu, 15 Sep 2016 12:09:50 -0600"},{"id":"fd9863bcf86b5ea51ffd939589770bae","data":"[START SESSION][SYSTEM]  Start Session on Thu, 15 Sep 16 16:19:46 -0600","date":"Thu, 15 Sep 2016 16:19:46 -0600"},{"id":"131a37ab08b13f9395c15619c78a474c","data":"[START SESSION][SYSTEM]  Start Session on Fri, 16 Sep 16 22:57:57 -0600","date":"Fri, 16 Sep 2016 22:57:57 -0600"},{"id":"e98548bb673381d37f79d44efdf4ba00","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Fri, 16 Sep 16 22:58:20 -0600","date":"Fri, 16 Sep 2016 22:58:20 -0600"},{"id":"ab425bb28980be721e4b460b9c951b3b","data":"[START SESSION][SYSTEM]  Start Session on Fri, 16 Sep 16 23:12:45 -0600","date":"Fri, 16 Sep 2016 23:12:45 -0600"},{"id":"50eaa7764af017cefa9b0c473512c931","data":"[START SESSION][SYSTEM]  Start Session on Sat, 17 Sep 16 08:59:39 -0600","date":"Sat, 17 Sep 2016 08:59:39 -0600"},{"id":"1fe747ab0b81c937583e0b0691534362","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 14:58:19 -0600","date":"Sun, 18 Sep 2016 14:58:19 -0600"},{"id":"4fd3e7fc7fcda6b6909bc6925b657732","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 14:58:27 -0600","date":"Sun, 18 Sep 2016 14:58:27 -0600"},{"id":"4bde0f291233c1b3e381b222d803471b","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 14:58:46 -0600","date":"Sun, 18 Sep 2016 14:58:46 -0600"},{"id":"b5a14a7a83ab3744ac12edf585396f01","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 14:59:29 -0600","date":"Sun, 18 Sep 2016 14:59:29 -0600"},{"id":"cf3b8e0d4fa200df52da3c3a9dcc4257","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 15:00:20 -0600","date":"Sun, 18 Sep 2016 15:00:20 -0600"},{"id":"184940624740fa6173a2cd0bcaa6b1f4","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sun, 18 Sep 16 15:00:39 -0600","date":"Sun, 18 Sep 2016 15:00:39 -0600"},{"id":"1a0dce5f13362c3095b9342025997a31","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 16:48:31 -0600","date":"Sun, 18 Sep 2016 16:48:31 -0600"},{"id":"b27af9e738bbc0d02ea18c4708ac070b","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sun, 18 Sep 16 16:52:33 -0600","date":"Sun, 18 Sep 2016 16:52:33 -0600"},{"id":"fc2c5f91d632b3366dec63f44245a97c","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 16:53:34 -0600","date":"Sun, 18 Sep 2016 16:53:34 -0600"},{"id":"54b6d51f965baaa95aba537ce0c41210","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sun, 18 Sep 16 16:54:18 -0600","date":"Sun, 18 Sep 2016 16:54:18 -0600"},{"id":"fb0901df5ecbc13f21f858a39bb084d3","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 19:20:08 -0600","date":"Sun, 18 Sep 2016 19:20:08 -0600"},{"id":"21b94c90d96bd9b64dacdc00ec35f5f1","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sun, 18 Sep 16 19:25:09 -0600","date":"Sun, 18 Sep 2016 19:25:09 -0600"},{"id":"c3f7259ccca07531f67f9ce88b3541f2","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 19:25:19 -0600","date":"Sun, 18 Sep 2016 19:25:19 -0600"},{"id":"9129d30d28b0d4e54e231f4917486c34","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 19:31:53 -0600","date":"Sun, 18 Sep 2016 19:31:53 -0600"},{"id":"fe33ec5b5778e47978b2af48ad6185fc","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 19:33:59 -0600","date":"Sun, 18 Sep 2016 19:33:59 -0600"},{"id":"1d596d88ff381b19afe48e963b176afb","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sun, 18 Sep 16 19:37:38 -0600","date":"Sun, 18 Sep 2016 19:37:38 -0600"},{"id":"e69209a2c61a8ce5b0c0328d690a15b5","data":"[START SESSION][SYSTEM]  Start Session on Sun, 18 Sep 16 19:39:50 -0600","date":"Sun, 18 Sep 2016 19:39:50 -0600"}]', 1, 0);
/*!40000 ALTER TABLE `ga_metadata` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_nav
CREATE TABLE IF NOT EXISTS `ga_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `route` varchar(50) DEFAULT NULL,
  `objects` text,
  `components` text,
  `parent` varchar(255) DEFAULT NULL,
  `origins` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `privs` varchar(255) DEFAULT '0',
  `token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COMMENT='navbar version 1.0 ';

-- Volcando datos para la tabla garrobo.ga_nav: 11 rows
/*!40000 ALTER TABLE `ga_nav` DISABLE KEYS */;
INSERT INTO `ga_nav` (`id`, `type`, `name`, `location`, `route`, `objects`, `components`, `parent`, `origins`, `active`, `privs`, `token`) VALUES
	(1, 'namespace', 'Menu', NULL, NULL, '{\n  \n  "icon" : "",\n  "redirect": "",\n  "target" : ""\n  \n}', '{}', '0', 'system', 1, '0', NULL),
	(2, 'sidebar', '{"es": "Inicio" , "en" : "Home" }', '', NULL, '{\r\n  \r\n  "icon" : "fa fa-home",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '1', 'system', 1, '0', NULL),
	(3, 'section', 'Menu', '', NULL, '{\r\n  \r\n  "icon" : "",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '1', 'system', 0, '0', NULL),
	(4, 'namespace', '{"es": "Administrador" , "en" : "Admin"}', NULL, NULL, '{\r\n  \r\n  "icon" : "",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '0', 'system', 1, '1,2', NULL),
	(5, 'section', '{"es": "Usuarios" , "en" : "Users" }', NULL, NULL, '{\r\n  \r\n  "icon" : "fa fa-users",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '4', 'system', 1, '1,2', NULL),
	(6, 'sidebar', 'Permisos', 'system=permission', 'permission', '{\r\n  \r\n  "icon" : "fa fa-terminal",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '5', 'system', 1, '1', NULL),
	(8, 'sub_menu', '{"es": "Mi perfil " , "en" : "My Profile"}', 'profile=profile', 'profile', '{\r\n  \r\n  "icon" : "icon-user",\r\n  "redirect": "",\r\n  "target" : "",\r\n "place" : "1",\r\n "divider" : "true"\r\n  \r\n}', '{}', '0', 'system', 1, '0', NULL),
	(9, 'sub_menu', '{"es": "Cerrar Sesión" , "en" : "Close Session" }', 'system=end_session', 'end_session', '{\r\n  \r\n  "icon" : "icon-key",\r\n  "redirect": "",\r\n  "target" : "",\r\n  "place" : "1"\r\n  \r\n}', '{}', '0', 'system', 1, '0', NULL),
	(10, 'sidebar', 'Profile', 'profile=profile', 'profile', '{\r\n  \r\n  "icon" : "icon-user",\r\n  "redirect": "",\r\n  "target" : "",\r\n "place" : "1",\r\n "divider" : "true"\r\n  \r\n}', '{}', '5', 'system', 1, '1,3', NULL),
	(13, 'section', '{"es": "Sistema" , "en" : "System" }', NULL, NULL, '{\r\n  \r\n  "icon" : "fa fa-cog",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '4', 'system', 1, '1', NULL),
	(14, 'sidebar', '{"es" : "Constantes" , "en" : "Constants" }', 'system=constants', 'system_constants', '{\r\n  \r\n  "icon" : "fa fa-pencil",\r\n  "redirect": "",\r\n  "target" : "",\r\n "place" : "1",\r\n "divider" : "true"\r\n  \r\n}', '{}', '13', 'system', 1, '1', NULL);
/*!40000 ALTER TABLE `ga_nav` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_notify
CREATE TABLE IF NOT EXISTS `ga_notify` (
  `id_notify` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `id_meta` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `content` varchar(255) NOT NULL,
  `reads` text NOT NULL,
  `active` int(11) DEFAULT '1',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `send_in_date` int(11) DEFAULT '0',
  PRIMARY KEY (`id_notify`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla garrobo.ga_notify: 0 rows
/*!40000 ALTER TABLE `ga_notify` DISABLE KEYS */;
/*!40000 ALTER TABLE `ga_notify` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_pages
CREATE TABLE IF NOT EXISTS `ga_pages` (
  `id_page` int(11) NOT NULL,
  `id_user` int(11) DEFAULT '0',
  `id_rol` int(11) DEFAULT '0',
  `id_meta` int(11) DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `url` varchar(250) NOT NULL,
  `content` mediumtext,
  `activate` int(11) DEFAULT '1',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `modify_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_page`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla garrobo.ga_pages: 0 rows
/*!40000 ALTER TABLE `ga_pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `ga_pages` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_rols
CREATE TABLE IF NOT EXISTS `ga_rols` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `meta` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='la tabla de rols es la mas sencilla solo especificamos un nombre al rol \r\ntalves algun meta agregado a ello pero nada mas ';

-- Volcando datos para la tabla garrobo.ga_rols: 3 rows
/*!40000 ALTER TABLE `ga_rols` DISABLE KEYS */;
INSERT INTO `ga_rols` (`id`, `name`, `meta`) VALUES
	(1, 'administrator', NULL),
	(2, 'guess', NULL),
	(3, 'user', NULL);
/*!40000 ALTER TABLE `ga_rols` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_sessions
CREATE TABLE IF NOT EXISTS `ga_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla garrobo.ga_sessions: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `ga_sessions` DISABLE KEYS */;
INSERT INTO `ga_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
	('4256ff91f654235d9cc216f24b56b42d16cc05b2', '127.0.0.1', 1473640094, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313437333634303039343B),
	('7ca66266366fc299adc40a6e4a594fe9222bd174', '127.0.0.1', 1473646788, _binary 0x757365727C4F3A383A22737464436C617373223A393A7B733A383A22757365726E616D65223B733A353A2261646D696E223B733A383A2270617373776F7264223B733A353A2261646D696E223B733A31323A226C6173745F636F6E6E656374223B733A31393A22323031362D30372D33312031393A33383A3335223B733A363A22616374697665223B733A313A2231223B733A343A2264617461223B4F3A383A22737464436C617373223A323A7B733A373A2264657461696C73223B4F3A383A22737464436C617373223A373A7B733A343A226E616D65223B733A31353A22526F6C616E646F20416E746F6E696F223B733A393A226C6173745F6E616D65223B733A31373A2241727269617A61204D6172726F7175696E223B733A383A227265676973746572223B733A31303A22323031362D30362D3139223B733A363A22617661746172223B733A34303A2238524B65773970452D6176617461722D67617272726F626F5F7369676E61747572655F312E6A7067223B733A31303A226F636375706174696F6E223B733A33393A22496E67656E6965726F20656E206369656E63696173206465206C6120636F6D7075746163696F6E223B733A383A226C6F636174696F6E223B733A31313A22456C2053616C7661646F72223B733A373A2277656273697465223B733A32323A227777772E726F6C616E646F61727269617A612E636F6D223B7D733A31343A226C6173745F70617373776F726473223B4F3A383A22737464436C617373223A303A7B7D7D733A353A227072697673223B4F3A383A22737464436C617373223A323A7B733A363A22706172656E74223B733A313A2231223B733A363A226368696C6473223B733A333A22322C33223B7D733A353A22656D61696C223B733A31393A22726F6C69676E75393040676D61696C2E636F6D223B733A323A226964223B733A313A2231223B733A343A226C616E67223B733A323A22656E223B7D),
	('503ea76a2cdac23e4f22b89672d6f9cc0f44a7a5', '127.0.0.1', 1473640422, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313437333634303432323B),
	('cbd80b423a5a8c43a920c4d0a7bcbc012db057fe', '127.0.0.1', 1473640748, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313437333634303734383B),
	('9127fa97b35cd59a81bdf54ffcd0763c3b10ba35', '127.0.0.1', 1473646044, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313437333634363034343B),
	('c12ac89b6b507f35a125639810c9a1cd714e314c', '127.0.0.1', 1473646479, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313437333634363437393B),
	('66d43dd91f40ce07d4775cff141ef35b21411ea7', '127.0.0.1', 1473646700, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313437333634363437393B),
	('38dfee2daebb4defe2d6fcc4e94cb8b648eeb2ff', '127.0.0.1', 1473646790, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313437333634363738333B);
/*!40000 ALTER TABLE `ga_sessions` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_user
CREATE TABLE IF NOT EXISTS `ga_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` text NOT NULL,
  `data` text,
  `privileges` text,
  `last_connect` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(2) DEFAULT NULL,
  `connected` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Garrobo CMS base de datos del usuario , se definieron estas bases de datos \r\nde una forma en la que la experciendia sea ,ucho mejor. en palabras mas \r\ncristianas la base de datos debe de responder de forma rapida.';

-- Volcando datos para la tabla garrobo.ga_user: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `ga_user` DISABLE KEYS */;
INSERT INTO `ga_user` (`id`, `username`, `email`, `password`, `data`, `privileges`, `last_connect`, `active`, `connected`) VALUES
	(1, 'admin', 'rolignu90@gmail.com', '7d7ff17eb8d7eb50d4e04c7ff096b291c0cdfa64a5d33d2ac5ef6fb253062d8ef29d7e82305db6df84f3f128bd415e9552131cfe488251c21e43c0aecf990fe0hBHnjDJeHG9ClaLf97Vc0KRyt/mC', '{"details":{"name":"Rolando Antonio","last_name":"Arriaza Marroquin","register":"2016-06-19","avatar":"TBVCj9Fd-avatar-dany dibujo.png","occupation":"Ingeniero en ciencias de la computacion","location":"El Salvador","website":"www.rolandoarriaza.com"},"last_passwords":{}}', '{\n\n    "parent" : "1",\n    "childs" : "2,3"\n  \n}', '2016-09-15 11:44:38', 1, 1),
	(2, 'guess', 'rolignu90@gmail.com', 'a384536d1770d68fcf51bf9bcf487fd03bc2f4d76af561736cdfd3206e7f2945815ee403b8433a1785bd9c7d8b146925e0a72eeeb9fc58a0182f5becb37599f7tZGCi96SUlNlnj4zBN34d5MdblJcucJaAMGX', '{"details":{"name":"No ahora porfavor ","last_name":"Arriaza Marroquin","register":"2016-06-19","avatar":"bIy6ZCc8-avatar-tetas-putas-desmotivaciones.jpg","occupation":"Ingeniero en sistemas","location":"El salvador","website":"www.rolandoarriaza.com"},"last_passwords":["admin","admin","admin","admin","admin","linux90"]}', '{\n\n    "parent" : "2",\n    "childs" : "3"\n  \n}', '2016-09-18 15:44:07', 1, 1);
/*!40000 ALTER TABLE `ga_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
