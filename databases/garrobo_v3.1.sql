-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.9 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para garrobo
CREATE DATABASE IF NOT EXISTS `garrobo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `garrobo`;


-- Volcando estructura para tabla garrobo.ga_metadata
CREATE TABLE IF NOT EXISTS `ga_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `value` longtext,
  `id_user` int(10) unsigned DEFAULT '0',
  `id_rol` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 COMMENT='metadata es una tabla en la cual sus valores no son estaticos , eso quiere decir que la tabla\r\nno es uso especifico asi que solo tiene unos pares de campos y la mayoria se trabajara\r\ncon variables o con cadenas cuyo patrones estan definidos aplica con JSON y XML';

-- Volcando datos para la tabla garrobo.ga_metadata: 4 rows
DELETE FROM `ga_metadata`;
/*!40000 ALTER TABLE `ga_metadata` DISABLE KEYS */;
INSERT INTO `ga_metadata` (`id`, `key`, `value`, `id_user`, `id_rol`) VALUES
	(12, 'user_lang', 'en', 1, 0),
	(3, 'log_file', '[{"id":"e14642183504bbc195a680e4a0e9adc3","data":"[START SESSION][SYSTEM]  Start Session on Sun, 10 Jul 16 22:36:26 -0600","date":"Sun, 10 Jul 2016 22:36:26 -0600"},{"id":"d7011beef53870d4b0d4df53e851e797","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sun, 10 Jul 16 22:36:51 -0600","date":"Sun, 10 Jul 2016 22:36:51 -0600"},{"id":"f93c1c9a16eb4fec3142bfc56448d37d","data":"[START SESSION][SYSTEM]  Start Session on Sun, 10 Jul 16 22:37:13 -0600","date":"Sun, 10 Jul 2016 22:37:13 -0600"},{"id":"587e3fc3e2b86b22d6b5f0171a0e879e","data":"[START SESSION][SYSTEM]  Start Session on Mon, 11 Jul 16 21:15:03 -0600","date":"Mon, 11 Jul 2016 21:15:03 -0600"},{"id":"b9f317aa0f519ed810304e1e15eba83a","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Mon, 11 Jul 16 21:29:42 -0600","date":"Mon, 11 Jul 2016 21:29:42 -0600"},{"id":"83bea63dd734fe5465794fc5da3767fb","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 19:34:43 -0600","date":"Tue, 12 Jul 2016 19:34:43 -0600"},{"id":"1aeb7110cc9457c93a3cbf82ae706b01","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 19:37:39 -0600","date":"Tue, 12 Jul 2016 19:37:39 -0600"},{"id":"7bb34fd728bebffd3e7983be888bcf7e","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 19:38:02 -0600","date":"Tue, 12 Jul 2016 19:38:02 -0600"},{"id":"57544980eba6a8320b5fc1f7bee83aab","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 20:27:39 -0600","date":"Tue, 12 Jul 2016 20:27:39 -0600"},{"id":"9c9bd76eb18713171ba33254ba2d465e","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 20:27:45 -0600","date":"Tue, 12 Jul 2016 20:27:45 -0600"},{"id":"fe7f6fe1ee622c343d1d8e38fe69fc2c","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 21:04:01 -0600","date":"Tue, 12 Jul 2016 21:04:01 -0600"},{"id":"a8f0d261f1fb3905a7ff8949993af163","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 21:04:07 -0600","date":"Tue, 12 Jul 2016 21:04:07 -0600"},{"id":"787ca1c35c0b47ab3a23c6ee540e5763","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 21:45:57 -0600","date":"Tue, 12 Jul 2016 21:45:57 -0600"},{"id":"d80bad5891219e40858097f953eabeed","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 21:46:03 -0600","date":"Tue, 12 Jul 2016 21:46:03 -0600"},{"id":"04d41e3089ffbf37e37879739b5a1ec8","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:46:33 -0600","date":"Tue, 12 Jul 2016 21:46:33 -0600"},{"id":"76d0efbf4cad3c1fc3cb5c9f38d14bc9","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:46:35 -0600","date":"Tue, 12 Jul 2016 21:46:35 -0600"},{"id":"ca7da569082822b1c73b7bb71d6cb675","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:46:37 -0600","date":"Tue, 12 Jul 2016 21:46:37 -0600"},{"id":"9273c5d88a6fb7215abb2d00f42a8632","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:54:03 -0600","date":"Tue, 12 Jul 2016 21:54:03 -0600"},{"id":"7ab32351b87861d13c02b864de8dd044","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:56:42 -0600","date":"Tue, 12 Jul 2016 21:56:42 -0600"},{"id":"ed7f19b2c567974fb539b8885a41f9b7","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 22:09:30 -0600","date":"Tue, 12 Jul 2016 22:09:30 -0600"},{"id":"d03b9d732731a29680c0d818558ce3da","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 22:09:36 -0600","date":"Tue, 12 Jul 2016 22:09:36 -0600"},{"id":"9fc1334cced2067b78d4aff7aef01186","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 22:26:22 -0600","date":"Tue, 12 Jul 2016 22:26:22 -0600"},{"id":"5b6a297e14156a3eb8faa2cfb5143a2b","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 22:26:40 -0600","date":"Tue, 12 Jul 2016 22:26:40 -0600"},{"id":"aea883b41fd3fe949144aa9e88611d52","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 23:09:03 -0600","date":"Tue, 12 Jul 2016 23:09:03 -0600"},{"id":"68366ba7fcff7a210929c587f4944af0","data":"[START SESSION][SYSTEM]  Start Session on Wed, 13 Jul 16 22:18:53 -0600","date":"Wed, 13 Jul 2016 22:18:53 -0600"},{"id":"2b3023d9acd742daf1e725e89f935996","data":"[START SESSION][SYSTEM]  Start Session on Thu, 14 Jul 16 20:33:53 -0600","date":"Thu, 14 Jul 2016 20:33:53 -0600"},{"id":"5984a3649dc7b9a94ee7ef4f6c4f68df","data":"[START SESSION][SYSTEM]  Start Session on Fri, 15 Jul 16 20:25:09 -0600","date":"Fri, 15 Jul 2016 20:25:09 -0600"},{"id":"374f830e55b10b4fcb8271c5d2ad964c","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Fri, 15 Jul 16 20:33:46 -0600","date":"Fri, 15 Jul 2016 20:33:46 -0600"},{"id":"d2a228893dc8c6d3e0657bb97c0b1403","data":"[START SESSION][SYSTEM]  Start Session on Sat, 16 Jul 16 09:16:07 -0600","date":"Sat, 16 Jul 2016 09:16:07 -0600"},{"id":"85d24f21edc833d7bb6aebae15c397cd","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sat, 16 Jul 16 09:25:47 -0600","date":"Sat, 16 Jul 2016 09:25:47 -0600"},{"id":"ca136393a3b8aeccaac2250f99a95440","data":"[START SESSION][SYSTEM]  Start Session on Sat, 16 Jul 16 09:25:54 -0600","date":"Sat, 16 Jul 2016 09:25:54 -0600"},{"id":"42e0639d6546a6b9332c1dd77ddefab2","data":"[START SESSION][SYSTEM]  Start Session on Sat, 16 Jul 16 22:51:47 -0600","date":"Sat, 16 Jul 2016 22:51:47 -0600"},{"id":"f42c8c9ecff68d5506a428138e302394","data":"[START SESSION][SYSTEM]  Start Session on Sun, 17 Jul 16 11:03:38 -0600","date":"Sun, 17 Jul 2016 11:03:38 -0600"},{"id":"c109cdce6b37e895c8e0cd6fdd8c7c6e","data":"[Edicion profile][SYSTEM] Rolando Antonio has modificado tus datos el dia Sun, 17 Jul 16 11:28:23 -0600","date":"Sun, 17 Jul 2016 11:28:23 -0600"},{"id":"3437cab2e612207e75a9d6e5c838913b","data":"[START SESSION][SYSTEM]  Start Session on Sun, 17 Jul 16 15:37:48 -0600","date":"Sun, 17 Jul 2016 15:37:48 -0600"},{"id":"1b22e0d8a81fc4c59b2db3a75bd3d408","data":"[Edicion profile][SYSTEM] Rolando Antonio has modificado tus datos el dia Sun, 17 Jul 16 18:17:02 -0600","date":"Sun, 17 Jul 2016 18:17:02 -0600"},{"id":"6a00ef330759fc75c184467004ac0705","data":"[Edicion profile][SYSTEM] Rolando Antonio has modificado tus datos el dia Sun, 17 Jul 16 19:03:15 -0600","date":"Sun, 17 Jul 2016 19:03:15 -0600"},{"id":"eca710cc03f1cbc059a95646f0042da2","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 18:41:41 -0600","date":"Mon, 18 Jul 2016 18:41:41 -0600"},{"id":"1486355d1533c02d463183e88bfdec1e","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:39:52 -0600","date":"Mon, 18 Jul 2016 19:39:52 -0600"},{"id":"153fb7427e7d427ba5bbba71d4630e3f","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:40:03 -0600","date":"Mon, 18 Jul 2016 19:40:03 -0600"},{"id":"720e18e689304764478c6a11359d06d9","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:43:59 -0600","date":"Mon, 18 Jul 2016 19:43:59 -0600"},{"id":"a7a5dfff2d43a80247afbb49b9712534","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:44:07 -0600","date":"Mon, 18 Jul 2016 19:44:07 -0600"},{"id":"e579e587eb87cb1e9d8bcc2fdaa5af42","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:44:49 -0600","date":"Mon, 18 Jul 2016 19:44:49 -0600"},{"id":"fd034ab4e75fc6abe516ed4031ade028","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:46:16 -0600","date":"Mon, 18 Jul 2016 19:46:16 -0600"},{"id":"132c1c02da5fd780ed5647095cf6ec1b","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:47:02 -0600","date":"Mon, 18 Jul 2016 19:47:02 -0600"},{"id":"6976fe9e0d5f224a84ebce6386af4fb0","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:47:12 -0600","date":"Mon, 18 Jul 2016 19:47:12 -0600"},{"id":"35f3854e2985246d74fe0786adf20ae9","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:47:42 -0600","date":"Mon, 18 Jul 2016 19:47:42 -0600"},{"id":"cb157afdc8851c6ea46a43e13736386f","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:47:48 -0600","date":"Mon, 18 Jul 2016 19:47:48 -0600"},{"id":"e60ed928de9b952ae2c99776680b050c","data":"[START SESSION][SYSTEM]  Start Session on Tue, 19 Jul 16 20:00:35 -0600","date":"Tue, 19 Jul 2016 20:00:35 -0600"},{"id":"d183d2980c692f98d31d89202af53932","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 19 Jul 16 22:30:32 -0600","date":"Tue, 19 Jul 2016 22:30:32 -0600"},{"id":"b429784f5e472c2966bec06e8308873c","data":"[START SESSION][SYSTEM]  Start Session on Tue, 19 Jul 16 22:30:47 -0600","date":"Tue, 19 Jul 2016 22:30:47 -0600"},{"id":"5b02f5a4e558edc9d3c068445c862636","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 19 Jul 16 22:30:59 -0600","date":"Tue, 19 Jul 2016 22:30:59 -0600"},{"id":"23ca95954dc353705b1d93a6b089c483","data":"[START SESSION][SYSTEM]  Start Session on Thu, 21 Jul 16 21:47:49 -0600","date":"Thu, 21 Jul 2016 21:47:49 -0600"},{"id":"3c083cb82319f58acc51e210d7108984","data":"[Edicion profile][SYSTEM] Rolando Antonio has modificado tus datos el dia Thu, 21 Jul 16 22:52:15 -0600","date":"Thu, 21 Jul 2016 22:52:15 -0600"},{"id":"7cc925d7bc64d1989ce5c1e965d70313","data":"[START SESSION][SYSTEM]  Start Session on Fri, 22 Jul 16 19:05:32 -0600","date":"Fri, 22 Jul 2016 19:05:32 -0600"},{"id":"15d340af728be2e3c79430a6d449bef4","data":"[Edicion profile][SYSTEM] Rolando Antonio has modificado tus datos el dia Fri, 22 Jul 16 23:23:13 -0600","date":"Fri, 22 Jul 2016 23:23:13 -0600"},{"id":"f41fc6f606fcc4c9ce7661f97b3c4c0e","data":"[START SESSION][SYSTEM]  Start Session on Sat, 23 Jul 16 08:39:41 -0600","date":"Sat, 23 Jul 2016 08:39:41 -0600"},{"id":"daffa9974f7a8139ee483a50713fa102","data":"[START SESSION][SYSTEM]  Start Session on Sun, 24 Jul 16 10:03:26 -0600","date":"Sun, 24 Jul 2016 10:03:26 -0600"},{"id":"cbaad395fd7d5cdc62cccbf9c58a8ff0","data":"[Edicion profile][SYSTEM] Rolando Antonio has modificado tus datos el dia Sun, 24 Jul 16 11:40:28 -0600","date":"Sun, 24 Jul 2016 11:40:28 -0600"},{"id":"fe247bfa8ddd0c8d6a1997d7849e7c12","data":"[START SESSION][SYSTEM]  Start Session on Sun, 24 Jul 16 22:53:27 -0600","date":"Sun, 24 Jul 2016 22:53:27 -0600"},{"id":"da6eac064772773ff570f3f22fa712c3","data":"[START SESSION][SYSTEM]  Start Session on Wed, 27 Jul 16 19:26:49 -0600","date":"Wed, 27 Jul 2016 19:26:49 -0600"},{"id":"8141437ae96a3d526d7fc9a1c7b13af9","data":"[Avatar ][SYSTEM] Rolando Antonio Avatar has been change in  Wed, 27 Jul 16 21:53:27 -0600","date":"Wed, 27 Jul 2016 21:53:27 -0600"},{"id":"475d11663f6a2ea00d2120707254a95f","data":"[START SESSION][SYSTEM]  Start Session on Thu, 28 Jul 16 20:08:21 -0600","date":"Thu, 28 Jul 2016 20:08:21 -0600"},{"id":"e979ed29e8c06fe6ce832ae66f5c8f66","data":"[START SESSION][SYSTEM]  Start Session on Sat, 30 Jul 16 16:58:00 -0600","date":"Sat, 30 Jul 2016 16:58:00 -0600"},{"id":"d00548a7d27c9650e4733564cecc94bf","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sat, 30 Jul 16 18:51:13 -0600","date":"Sat, 30 Jul 2016 18:51:13 -0600"},{"id":"ac7152985a245d1398ef60900b255956","data":"[START SESSION][SYSTEM]  Start Session on Sat, 30 Jul 16 18:51:22 -0600","date":"Sat, 30 Jul 2016 18:51:22 -0600"},{"id":"16808340dbb3144f9539ef075ab82de8","data":"[START SESSION][SYSTEM]  Start Session on Sun, 31 Jul 16 18:54:37 -0600","date":"Sun, 31 Jul 2016 18:54:37 -0600"},{"id":"cf40dc22e595c1138707605892efaa6c","data":"[START SESSION][SYSTEM]  Start Session on Sun, 31 Jul 16 18:54:38 -0600","date":"Sun, 31 Jul 2016 18:54:38 -0600"},{"id":"2a0ff46baa001de1f6e587bd01ed0dc0","data":"[Avatar ][SYSTEM] Rolando Antonio Avatar has been change in  Sun, 31 Jul 16 19:38:08 -0600","date":"Sun, 31 Jul 2016 19:38:08 -0600"},{"id":"d9f765f1b3902f299da026aa1ca1a6ef","data":"[Avatar ][SYSTEM] Rolando Antonio Avatar has been change in  Sun, 31 Jul 16 19:38:36 -0600","date":"Sun, 31 Jul 2016 19:38:36 -0600"},{"id":"21c6897972252d7b8d1c81f0fbbcc3e3","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sun, 31 Jul 16 19:40:45 -0600","date":"Sun, 31 Jul 2016 19:40:45 -0600"},{"id":"567d391dabd4d4b2dc00d9b131a51422","data":"[START SESSION][SYSTEM]  Start Session on Sun, 31 Jul 16 20:57:07 -0600","date":"Sun, 31 Jul 2016 20:57:07 -0600"},{"id":"0bd16918472dc9da3659629246d69497","data":"[START SESSION][SYSTEM]  Start Session on Thu, 04 Aug 16 13:55:04 -0600","date":"Thu, 04 Aug 2016 13:55:04 -0600"},{"id":"a0a870ee80551b09b53fe7b9646ca21a","data":"[START SESSION][SYSTEM]  Start Session on Fri, 12 Aug 16 21:45:10 -0600","date":"Fri, 12 Aug 2016 21:45:10 -0600"},{"id":"aef8c2ac8ade79cccc6c4a71aae436fd","data":"[START SESSION][SYSTEM]  Start Session on Sun, 21 Aug 16 11:03:14 -0600","date":"Sun, 21 Aug 2016 11:03:14 -0600"},{"id":"14f132d048a8c1d81aa0336d2f3a9a77","data":"[START SESSION][SYSTEM]  Start Session on Sun, 21 Aug 16 11:03:22 -0600","date":"Sun, 21 Aug 2016 11:03:22 -0600"},{"id":"536c3de79cc7d429eb46fe5b9936ba6b","data":"[START SESSION][SYSTEM]  Start Session on Sun, 21 Aug 16 11:03:32 -0600","date":"Sun, 21 Aug 2016 11:03:32 -0600"},{"id":"90d1184079e200f5f515e180382a287a","data":"[START SESSION][SYSTEM]  Start Session on Sun, 21 Aug 16 11:03:51 -0600","date":"Sun, 21 Aug 2016 11:03:51 -0600"},{"id":"290e27a077bcecc6e647b07bccdae00f","data":"[START SESSION][SYSTEM]  Start Session on Sun, 21 Aug 16 11:04:37 -0600","date":"Sun, 21 Aug 2016 11:04:37 -0600"},{"id":"3bce357f6f4c24a8fb21502b9b39e0a1","data":"[START SESSION][SYSTEM]  Start Session on Sun, 21 Aug 16 11:04:44 -0600","date":"Sun, 21 Aug 2016 11:04:44 -0600"},{"id":"1288eeb04d268f05dcb6b623a4742cf5","data":"[START SESSION][SYSTEM]  Start Session on Sun, 21 Aug 16 11:07:04 -0600","date":"Sun, 21 Aug 2016 11:07:04 -0600"},{"id":"922fdd411e95d4e3693dd23772ec16ed","data":"[START SESSION][SYSTEM]  Start Session on Sun, 21 Aug 16 11:07:49 -0600","date":"Sun, 21 Aug 2016 11:07:49 -0600"},{"id":"705571f8deec2531bbd8f51e8b76b849","data":"[START SESSION][SYSTEM]  Start Session on Sun, 21 Aug 16 11:12:44 -0600","date":"Sun, 21 Aug 2016 11:12:44 -0600"},{"id":"535442a983b76a8d2d8c60133901fe1c","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sun, 21 Aug 16 11:33:48 -0600","date":"Sun, 21 Aug 2016 11:33:48 -0600"},{"id":"cdede184ea7128d73a7fdcc0edaaf5ce","data":"[START SESSION][SYSTEM]  Start Session on Wed, 31 Aug 16 18:06:27 -0600","date":"Wed, 31 Aug 2016 18:06:27 -0600"}]', 1, 0),
	(21, 'user_lang', 'en', 2, 0),
	(22, 'log_file', '[{"id":"d1f2adb2ffa5c3376732a2d45b0d5a04","data":"[START SESSION][SYSTEM]  Start Session on Wed, 03 Aug 16 17:44:51 -0600","date":"Wed, 03 Aug 2016 17:44:51 -0600"},{"id":"1a7d0dedabd668f97616dfe96d2b366c","data":"[Avatar ][SYSTEM] Rolando Antonio Avatar has been change in  Wed, 03 Aug 16 17:45:43 -0600","date":"Wed, 03 Aug 2016 17:45:43 -0600"},{"id":"b1286c5329010d1f8b2f7e17e4611e2b","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Wed, 03 Aug 16 18:29:28 -0600","date":"Wed, 03 Aug 2016 18:29:28 -0600"},{"id":"96baf10427597f7333c598eb7aa9ccfe","data":"[START SESSION][SYSTEM]  Start Session on Wed, 03 Aug 16 18:29:37 -0600","date":"Wed, 03 Aug 2016 18:29:37 -0600"},{"id":"d546787ddc1b131c28345bc180339bc0","data":"[Edicion profile][SYSTEM] No ahora porfavor  has modificado tus datos el dia Wed, 03 Aug 16 19:38:05 -0600","date":"Wed, 03 Aug 2016 19:38:05 -0600"},{"id":"a4e7cbc7e8f21f06fefcc8f384a273bc","data":"[Avatar ][SYSTEM] No ahora porfavor  Avatar has been change in  Wed, 03 Aug 16 19:38:47 -0600","date":"Wed, 03 Aug 2016 19:38:47 -0600"},{"id":"458e7d294280da6b2883465e9594f3da","data":"[Avatar ][SYSTEM] No ahora porfavor  Avatar has been change in  Wed, 03 Aug 16 19:38:47 -0600","date":"Wed, 03 Aug 2016 19:38:47 -0600"},{"id":"5b15cd907c6889f7fb45e37478c496ae","data":"[End Session][SYSTEM] No ahora porfavor  has cerrado sesion la fecha de Wed, 03 Aug 16 21:08:51 -0600","date":"Wed, 03 Aug 2016 21:08:51 -0600"},{"id":"ba6af2e17bbf12473c8e7d10c8195c41","data":"[START SESSION][SYSTEM]  Start Session on Wed, 03 Aug 16 21:09:04 -0600","date":"Wed, 03 Aug 2016 21:09:04 -0600"},{"id":"6706b9a4650f18e7ac847494ac4ac1f9","data":"[End Session][SYSTEM] No ahora porfavor  has cerrado sesion la fecha de Wed, 03 Aug 16 21:40:22 -0600","date":"Wed, 03 Aug 2016 21:40:22 -0600"},{"id":"9194bd3ecfdd42c7a5804b73e9a83e3d","data":"[START SESSION][SYSTEM]  Start Session on Wed, 03 Aug 16 21:41:35 -0600","date":"Wed, 03 Aug 2016 21:41:35 -0600"},{"id":"ffda5768b605595635ad1a3baf835887","data":"[End Session][SYSTEM] No ahora porfavor  has cerrado sesion la fecha de Wed, 03 Aug 16 21:50:16 -0600","date":"Wed, 03 Aug 2016 21:50:16 -0600"},{"id":"de0c3419c6c302177b13349933ab48f2","data":"[START SESSION][SYSTEM]  Start Session on Thu, 04 Aug 16 13:51:18 -0600","date":"Thu, 04 Aug 2016 13:51:18 -0600"},{"id":"47fd1e4ea973523b25e5f001d11a8078","data":"[End Session][SYSTEM] No ahora porfavor  has cerrado sesion la fecha de Thu, 04 Aug 16 13:54:54 -0600","date":"Thu, 04 Aug 2016 13:54:54 -0600"}]', 2, 0);
/*!40000 ALTER TABLE `ga_metadata` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_nav
CREATE TABLE IF NOT EXISTS `ga_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `route` varchar(50) DEFAULT NULL,
  `objects` text,
  `components` text,
  `parent` varchar(255) DEFAULT NULL,
  `origins` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `privs` varchar(255) DEFAULT '0',
  `token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COMMENT='navbar version 1.0 ';

-- Volcando datos para la tabla garrobo.ga_nav: 9 rows
DELETE FROM `ga_nav`;
/*!40000 ALTER TABLE `ga_nav` DISABLE KEYS */;
INSERT INTO `ga_nav` (`id`, `type`, `name`, `location`, `route`, `objects`, `components`, `parent`, `origins`, `active`, `privs`, `token`) VALUES
	(1, 'namespace', 'Menu', NULL, NULL, '{\n  \n  "icon" : "",\n  "redirect": "",\n  "target" : ""\n  \n}', '{}', '0', 'system', 1, '0', NULL),
	(2, 'sidebar', '{"es": "Inicio" , "en" : "Home" }', '', NULL, '{\r\n  \r\n  "icon" : "fa fa-home",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '1', 'system', 1, '0', NULL),
	(3, 'section', 'Menu', '', NULL, '{\r\n  \r\n  "icon" : "",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '1', 'system', 0, '0', NULL),
	(4, 'namespace', '{"es": "Administrador" , "en" : "Admin"}', NULL, NULL, '{\r\n  \r\n  "icon" : "",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '0', 'system', 1, '1', NULL),
	(5, 'section', '{"es": "Usuarios" , "en" : "Users" }', NULL, NULL, '{\r\n  \r\n  "icon" : "fa fa-users",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '4', 'system', 1, '1', NULL),
	(6, 'sidebar', 'Permisos', 'system=permission', 'permission', '{\r\n  \r\n  "icon" : "fa fa-terminal",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '5', 'system', 1, '1', NULL),
	(8, 'sub_menu', '{"es": "Mi perfil " , "en" : "My Profile"}', 'profile=profile', 'profile', '{\r\n  \r\n  "icon" : "icon-user",\r\n  "redirect": "",\r\n  "target" : "",\r\n "place" : "1",\r\n "divider" : "true"\r\n  \r\n}', '{}', '0', 'system', 1, '1', NULL),
	(9, 'sub_menu', '{"es": "Cerrar Sesión" , "en" : "Close Session" }', 'system=end_session', 'end_session', '{\r\n  \r\n  "icon" : "icon-key",\r\n  "redirect": "",\r\n  "target" : "",\r\n  "place" : "1"\r\n  \r\n}', '{}', '0', 'system', 1, '1', NULL),
	(10, 'sidebar', 'Profile', 'profile=profile', 'profile', '{\r\n  \r\n  "icon" : "icon-user",\r\n  "redirect": "",\r\n  "target" : "",\r\n "place" : "1",\r\n "divider" : "true"\r\n  \r\n}', '{}', '5', 'system', 1, '1', NULL);
/*!40000 ALTER TABLE `ga_nav` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_notify
CREATE TABLE IF NOT EXISTS `ga_notify` (
  `id_notify` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `id_meta` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `content` varchar(255) NOT NULL,
  `reads` text NOT NULL,
  `active` int(11) DEFAULT '1',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `send_in_date` int(11) DEFAULT '0',
  PRIMARY KEY (`id_notify`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla garrobo.ga_notify: 0 rows
DELETE FROM `ga_notify`;
/*!40000 ALTER TABLE `ga_notify` DISABLE KEYS */;
/*!40000 ALTER TABLE `ga_notify` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_pages
CREATE TABLE IF NOT EXISTS `ga_pages` (
  `id_page` int(11) NOT NULL,
  `id_user` int(11) DEFAULT '0',
  `id_rol` int(11) DEFAULT '0',
  `id_meta` int(11) DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `url` varchar(250) NOT NULL,
  `content` mediumtext,
  `activate` int(11) DEFAULT '1',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `modify_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_page`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla garrobo.ga_pages: 0 rows
DELETE FROM `ga_pages`;
/*!40000 ALTER TABLE `ga_pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `ga_pages` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_rols
CREATE TABLE IF NOT EXISTS `ga_rols` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `meta` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='la tabla de rols es la mas sencilla solo especificamos un nombre al rol \r\ntalves algun meta agregado a ello pero nada mas ';

-- Volcando datos para la tabla garrobo.ga_rols: 3 rows
DELETE FROM `ga_rols`;
/*!40000 ALTER TABLE `ga_rols` DISABLE KEYS */;
INSERT INTO `ga_rols` (`id`, `name`, `meta`) VALUES
	(1, 'administrator', NULL),
	(2, 'guess', NULL),
	(3, 'user', NULL);
/*!40000 ALTER TABLE `ga_rols` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_user
CREATE TABLE IF NOT EXISTS `ga_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` text NOT NULL,
  `data` text,
  `privileges` text,
  `last_connect` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(2) DEFAULT NULL,
  `connected` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Garrobo CMS base de datos del usuario , se definieron estas bases de datos \r\nde una forma en la que la experciendia sea ,ucho mejor. en palabras mas \r\ncristianas la base de datos debe de responder de forma rapida.';

-- Volcando datos para la tabla garrobo.ga_user: ~0 rows (aproximadamente)
DELETE FROM `ga_user`;
/*!40000 ALTER TABLE `ga_user` DISABLE KEYS */;
INSERT INTO `ga_user` (`id`, `username`, `email`, `password`, `data`, `privileges`, `last_connect`, `active`, `connected`) VALUES
	(1, 'admin', 'rolignu90@gmail.com', '7d7ff17eb8d7eb50d4e04c7ff096b291c0cdfa64a5d33d2ac5ef6fb253062d8ef29d7e82305db6df84f3f128bd415e9552131cfe488251c21e43c0aecf990fe0hBHnjDJeHG9ClaLf97Vc0KRyt/mC', '{"details":{"name":"Rolando Antonio","last_name":"Arriaza Marroquin","register":"2016-06-19","avatar":"8RKew9pE-avatar-garrrobo_signature_1.jpg","occupation":"Ingeniero en ciencias de la computacion","location":"El Salvador","website":"www.rolandoarriaza.com"},"last_passwords":{}}', '{\n\n    "parent" : "1",\n    "childs" : "2,3"\n  \n}', '2016-07-31 19:38:35', 1, 1),
	(2, 'guess', 'rolignu90@gmail.com', 'a384536d1770d68fcf51bf9bcf487fd03bc2f4d76af561736cdfd3206e7f2945815ee403b8433a1785bd9c7d8b146925e0a72eeeb9fc58a0182f5becb37599f7tZGCi96SUlNlnj4zBN34d5MdblJcucJaAMGX', '{"details":{"name":"No ahora porfavor ","last_name":"Arriaza Marroquin","register":"2016-06-19","avatar":"TrnhlFAK-avatar-garrrobo_signature_1.jpg","occupation":"Ingeniero en sistemas","location":"El salvador","website":"www.rolandoarriaza.com"},"last_passwords":["admin","admin","admin","admin","admin","linux90"]}', '{\n\n    "parent" : "1",\n    "childs" : "2,3"\n  \n}', '2016-08-03 21:40:01', 1, 1);
/*!40000 ALTER TABLE `ga_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
