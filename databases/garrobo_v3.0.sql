-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.9 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para garrobo
CREATE DATABASE IF NOT EXISTS `garrobo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `garrobo`;


-- Volcando estructura para tabla garrobo.ga_metadata
CREATE TABLE IF NOT EXISTS `ga_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `value` longtext,
  `id_user` int(10) unsigned DEFAULT '0',
  `id_rol` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='metadata es una tabla en la cual sus valores no son estaticos , eso quiere decir que la tabla\r\nno es uso especifico asi que solo tiene unos pares de campos y la mayoria se trabajara\r\ncon variables o con cadenas cuyo patrones estan definidos aplica con JSON y XML';

-- Volcando datos para la tabla garrobo.ga_metadata: 2 rows
DELETE FROM `ga_metadata`;
/*!40000 ALTER TABLE `ga_metadata` DISABLE KEYS */;
INSERT INTO `ga_metadata` (`id`, `key`, `value`, `id_user`, `id_rol`) VALUES
	(12, 'user_lang', 'en', 1, 0),
	(3, 'log_file', '[{"id":"e14642183504bbc195a680e4a0e9adc3","data":"[START SESSION][SYSTEM]  Start Session on Sun, 10 Jul 16 22:36:26 -0600","date":"Sun, 10 Jul 2016 22:36:26 -0600"},{"id":"d7011beef53870d4b0d4df53e851e797","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sun, 10 Jul 16 22:36:51 -0600","date":"Sun, 10 Jul 2016 22:36:51 -0600"},{"id":"f93c1c9a16eb4fec3142bfc56448d37d","data":"[START SESSION][SYSTEM]  Start Session on Sun, 10 Jul 16 22:37:13 -0600","date":"Sun, 10 Jul 2016 22:37:13 -0600"},{"id":"587e3fc3e2b86b22d6b5f0171a0e879e","data":"[START SESSION][SYSTEM]  Start Session on Mon, 11 Jul 16 21:15:03 -0600","date":"Mon, 11 Jul 2016 21:15:03 -0600"},{"id":"b9f317aa0f519ed810304e1e15eba83a","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Mon, 11 Jul 16 21:29:42 -0600","date":"Mon, 11 Jul 2016 21:29:42 -0600"},{"id":"83bea63dd734fe5465794fc5da3767fb","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 19:34:43 -0600","date":"Tue, 12 Jul 2016 19:34:43 -0600"},{"id":"1aeb7110cc9457c93a3cbf82ae706b01","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 19:37:39 -0600","date":"Tue, 12 Jul 2016 19:37:39 -0600"},{"id":"7bb34fd728bebffd3e7983be888bcf7e","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 19:38:02 -0600","date":"Tue, 12 Jul 2016 19:38:02 -0600"},{"id":"57544980eba6a8320b5fc1f7bee83aab","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 20:27:39 -0600","date":"Tue, 12 Jul 2016 20:27:39 -0600"},{"id":"9c9bd76eb18713171ba33254ba2d465e","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 20:27:45 -0600","date":"Tue, 12 Jul 2016 20:27:45 -0600"},{"id":"fe7f6fe1ee622c343d1d8e38fe69fc2c","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 21:04:01 -0600","date":"Tue, 12 Jul 2016 21:04:01 -0600"},{"id":"a8f0d261f1fb3905a7ff8949993af163","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 21:04:07 -0600","date":"Tue, 12 Jul 2016 21:04:07 -0600"},{"id":"787ca1c35c0b47ab3a23c6ee540e5763","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 21:45:57 -0600","date":"Tue, 12 Jul 2016 21:45:57 -0600"},{"id":"d80bad5891219e40858097f953eabeed","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 21:46:03 -0600","date":"Tue, 12 Jul 2016 21:46:03 -0600"},{"id":"04d41e3089ffbf37e37879739b5a1ec8","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:46:33 -0600","date":"Tue, 12 Jul 2016 21:46:33 -0600"},{"id":"76d0efbf4cad3c1fc3cb5c9f38d14bc9","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:46:35 -0600","date":"Tue, 12 Jul 2016 21:46:35 -0600"},{"id":"ca7da569082822b1c73b7bb71d6cb675","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:46:37 -0600","date":"Tue, 12 Jul 2016 21:46:37 -0600"},{"id":"9273c5d88a6fb7215abb2d00f42a8632","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:54:03 -0600","date":"Tue, 12 Jul 2016 21:54:03 -0600"},{"id":"7ab32351b87861d13c02b864de8dd044","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Tue, 12 Jul 16 21:56:42 -0600","date":"Tue, 12 Jul 2016 21:56:42 -0600"},{"id":"ed7f19b2c567974fb539b8885a41f9b7","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 22:09:30 -0600","date":"Tue, 12 Jul 2016 22:09:30 -0600"},{"id":"d03b9d732731a29680c0d818558ce3da","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 22:09:36 -0600","date":"Tue, 12 Jul 2016 22:09:36 -0600"},{"id":"9fc1334cced2067b78d4aff7aef01186","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 22:26:22 -0600","date":"Tue, 12 Jul 2016 22:26:22 -0600"},{"id":"5b6a297e14156a3eb8faa2cfb5143a2b","data":"[START SESSION][SYSTEM]  Start Session on Tue, 12 Jul 16 22:26:40 -0600","date":"Tue, 12 Jul 2016 22:26:40 -0600"},{"id":"aea883b41fd3fe949144aa9e88611d52","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Tue, 12 Jul 16 23:09:03 -0600","date":"Tue, 12 Jul 2016 23:09:03 -0600"},{"id":"68366ba7fcff7a210929c587f4944af0","data":"[START SESSION][SYSTEM]  Start Session on Wed, 13 Jul 16 22:18:53 -0600","date":"Wed, 13 Jul 2016 22:18:53 -0600"},{"id":"2b3023d9acd742daf1e725e89f935996","data":"[START SESSION][SYSTEM]  Start Session on Thu, 14 Jul 16 20:33:53 -0600","date":"Thu, 14 Jul 2016 20:33:53 -0600"},{"id":"5984a3649dc7b9a94ee7ef4f6c4f68df","data":"[START SESSION][SYSTEM]  Start Session on Fri, 15 Jul 16 20:25:09 -0600","date":"Fri, 15 Jul 2016 20:25:09 -0600"},{"id":"374f830e55b10b4fcb8271c5d2ad964c","data":"[Edicion perfil][SYSTEM] Rolando Antonio has modificado tus datos el dia Fri, 15 Jul 16 20:33:46 -0600","date":"Fri, 15 Jul 2016 20:33:46 -0600"},{"id":"d2a228893dc8c6d3e0657bb97c0b1403","data":"[START SESSION][SYSTEM]  Start Session on Sat, 16 Jul 16 09:16:07 -0600","date":"Sat, 16 Jul 2016 09:16:07 -0600"},{"id":"85d24f21edc833d7bb6aebae15c397cd","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Sat, 16 Jul 16 09:25:47 -0600","date":"Sat, 16 Jul 2016 09:25:47 -0600"},{"id":"ca136393a3b8aeccaac2250f99a95440","data":"[START SESSION][SYSTEM]  Start Session on Sat, 16 Jul 16 09:25:54 -0600","date":"Sat, 16 Jul 2016 09:25:54 -0600"},{"id":"42e0639d6546a6b9332c1dd77ddefab2","data":"[START SESSION][SYSTEM]  Start Session on Sat, 16 Jul 16 22:51:47 -0600","date":"Sat, 16 Jul 2016 22:51:47 -0600"},{"id":"f42c8c9ecff68d5506a428138e302394","data":"[START SESSION][SYSTEM]  Start Session on Sun, 17 Jul 16 11:03:38 -0600","date":"Sun, 17 Jul 2016 11:03:38 -0600"},{"id":"c109cdce6b37e895c8e0cd6fdd8c7c6e","data":"[Edicion profile][SYSTEM] Rolando Antonio has modificado tus datos el dia Sun, 17 Jul 16 11:28:23 -0600","date":"Sun, 17 Jul 2016 11:28:23 -0600"},{"id":"3437cab2e612207e75a9d6e5c838913b","data":"[START SESSION][SYSTEM]  Start Session on Sun, 17 Jul 16 15:37:48 -0600","date":"Sun, 17 Jul 2016 15:37:48 -0600"},{"id":"1b22e0d8a81fc4c59b2db3a75bd3d408","data":"[Edicion profile][SYSTEM] Rolando Antonio has modificado tus datos el dia Sun, 17 Jul 16 18:17:02 -0600","date":"Sun, 17 Jul 2016 18:17:02 -0600"},{"id":"6a00ef330759fc75c184467004ac0705","data":"[Edicion profile][SYSTEM] Rolando Antonio has modificado tus datos el dia Sun, 17 Jul 16 19:03:15 -0600","date":"Sun, 17 Jul 2016 19:03:15 -0600"},{"id":"eca710cc03f1cbc059a95646f0042da2","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 18:41:41 -0600","date":"Mon, 18 Jul 2016 18:41:41 -0600"},{"id":"1486355d1533c02d463183e88bfdec1e","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:39:52 -0600","date":"Mon, 18 Jul 2016 19:39:52 -0600"},{"id":"153fb7427e7d427ba5bbba71d4630e3f","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:40:03 -0600","date":"Mon, 18 Jul 2016 19:40:03 -0600"},{"id":"720e18e689304764478c6a11359d06d9","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:43:59 -0600","date":"Mon, 18 Jul 2016 19:43:59 -0600"},{"id":"a7a5dfff2d43a80247afbb49b9712534","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:44:07 -0600","date":"Mon, 18 Jul 2016 19:44:07 -0600"},{"id":"e579e587eb87cb1e9d8bcc2fdaa5af42","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:44:49 -0600","date":"Mon, 18 Jul 2016 19:44:49 -0600"},{"id":"fd034ab4e75fc6abe516ed4031ade028","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:46:16 -0600","date":"Mon, 18 Jul 2016 19:46:16 -0600"},{"id":"132c1c02da5fd780ed5647095cf6ec1b","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:47:02 -0600","date":"Mon, 18 Jul 2016 19:47:02 -0600"},{"id":"6976fe9e0d5f224a84ebce6386af4fb0","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:47:12 -0600","date":"Mon, 18 Jul 2016 19:47:12 -0600"},{"id":"35f3854e2985246d74fe0786adf20ae9","data":"[End Session][SYSTEM] Rolando Antonio has cerrado sesion la fecha de Mon, 18 Jul 16 19:47:42 -0600","date":"Mon, 18 Jul 2016 19:47:42 -0600"},{"id":"cb157afdc8851c6ea46a43e13736386f","data":"[START SESSION][SYSTEM]  Start Session on Mon, 18 Jul 16 19:47:48 -0600","date":"Mon, 18 Jul 2016 19:47:48 -0600"},{"id":"e60ed928de9b952ae2c99776680b050c","data":"[START SESSION][SYSTEM]  Start Session on Tue, 19 Jul 16 20:00:35 -0600","date":"Tue, 19 Jul 2016 20:00:35 -0600"}]', 1, 0);
/*!40000 ALTER TABLE `ga_metadata` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_nav
CREATE TABLE IF NOT EXISTS `ga_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `route` varchar(50) DEFAULT NULL,
  `objects` text,
  `components` text,
  `parent` varchar(255) DEFAULT NULL,
  `origins` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `privs` varchar(255) DEFAULT '0',
  `token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='navbar version 1.0 ';

-- Volcando datos para la tabla garrobo.ga_nav: 8 rows
DELETE FROM `ga_nav`;
/*!40000 ALTER TABLE `ga_nav` DISABLE KEYS */;
INSERT INTO `ga_nav` (`id`, `type`, `name`, `location`, `route`, `objects`, `components`, `parent`, `origins`, `active`, `privs`, `token`) VALUES
	(1, 'namespace', 'Menu', NULL, NULL, '{\n  \n  "icon" : "",\n  "redirect": "",\n  "target" : ""\n  \n}', '{}', '0', 'system', 1, '0', NULL),
	(2, 'sidebar', 'Home', '', NULL, '{\r\n  \r\n  "icon" : "fa fa-home",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '1', 'system', 1, '0', NULL),
	(3, 'section', 'Menu', '', NULL, '{\r\n  \r\n  "icon" : "",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '1', 'system', 0, '0', NULL),
	(4, 'namespace', 'Administrador', NULL, NULL, '{\r\n  \r\n  "icon" : "",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '0', 'system', 1, '1', NULL),
	(5, 'section', 'Usuarios', NULL, NULL, '{\r\n  \r\n  "icon" : "fa fa-users",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '4', 'system', 1, '1', NULL),
	(6, 'sidebar', 'Permisos', 'system=permission', 'permission', '{\r\n  \r\n  "icon" : "fa fa-terminal",\r\n  "redirect": "",\r\n  "target" : ""\r\n  \r\n}', '{}', '5', 'system', 1, '1', NULL),
	(8, 'sub_menu', 'Mi perfil ', 'profile=profile', 'profile', '{\r\n  \r\n  "icon" : "icon-user",\r\n  "redirect": "",\r\n  "target" : "",\r\n "place" : "1",\r\n "divider" : "true"\r\n  \r\n}', '{}', '0', 'system', 1, '1', NULL),
	(9, 'sub_menu', 'Cerrar Sesión', 'system=end_session', 'end_session', '{\r\n  \r\n  "icon" : "icon-key",\r\n  "redirect": "",\r\n  "target" : "",\r\n  "place" : "1"\r\n  \r\n}', '{}', '0', 'system', 1, '1', NULL);
/*!40000 ALTER TABLE `ga_nav` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_rols
CREATE TABLE IF NOT EXISTS `ga_rols` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `meta` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='la tabla de rols es la mas sencilla solo especificamos un nombre al rol \r\ntalves algun meta agregado a ello pero nada mas ';

-- Volcando datos para la tabla garrobo.ga_rols: 3 rows
DELETE FROM `ga_rols`;
/*!40000 ALTER TABLE `ga_rols` DISABLE KEYS */;
INSERT INTO `ga_rols` (`id`, `name`, `meta`) VALUES
	(1, 'administrator', NULL),
	(2, 'guess', NULL),
	(3, 'user', NULL);
/*!40000 ALTER TABLE `ga_rols` ENABLE KEYS */;


-- Volcando estructura para tabla garrobo.ga_user
CREATE TABLE IF NOT EXISTS `ga_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` text NOT NULL,
  `data` text,
  `privileges` text,
  `last_connect` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(2) DEFAULT NULL,
  `connected` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Garrobo CMS base de datos del usuario , se definieron estas bases de datos \r\nde una forma en la que la experciendia sea ,ucho mejor. en palabras mas \r\ncristianas la base de datos debe de responder de forma rapida.';

-- Volcando datos para la tabla garrobo.ga_user: ~0 rows (aproximadamente)
DELETE FROM `ga_user`;
/*!40000 ALTER TABLE `ga_user` DISABLE KEYS */;
INSERT INTO `ga_user` (`id`, `username`, `email`, `password`, `data`, `privileges`, `last_connect`, `active`, `connected`) VALUES
	(1, 'admin', 'rolignu90@gmail.com', '7d7ff17eb8d7eb50d4e04c7ff096b291c0cdfa64a5d33d2ac5ef6fb253062d8ef29d7e82305db6df84f3f128bd415e9552131cfe488251c21e43c0aecf990fe0hBHnjDJeHG9ClaLf97Vc0KRyt/mC', '{"details":{"name":"Rolando Antonio","last_name":"Arriaza Marroquin","register":"2016-06-19","avatar":"","occupation":"Ingeniero en ciencias de la computacion","location":"El Salvador","website":"www.rolandoarriaza.com"},"last_passwords":{}}', '{\n\n    "parent" : "1",\n    "childs" : "2,3"\n  \n}', '2016-07-12 22:26:40', 1, 1),
	(2, 'guess', 'rolignu90@gmail.com', '7d7ff17eb8d7eb50d4e04c7ff096b291c0cdfa64a5d33d2ac5ef6fb253062d8ef29d7e82305db6df84f3f128bd415e9552131cfe488251c21e43c0aecf990fe0hBHnjDJeHG9ClaLf97Vc0KRyt/mC', '{\r\n  \r\n  "details" : \r\n  {\r\n     "name" : "Rolando Antonio",\r\n     "last_name" : "Arriaza Marroquin",\r\n     "register" : "2016-06-19",\r\n     "avatar" : "",\r\n     "occupation": "Ingeniero en sistemas",\r\n     "location": "El salvador",\r\n    "website" : "www.rolandoarriaza.com"\r\n  },\r\n  \r\n  "last_passwords": {}\r\n  \r\n}', '{\n\n    "parent" : "1",\n    "childs" : "2,3"\n  \n}', '2016-07-12 22:26:22', 1, 0);
/*!40000 ALTER TABLE `ga_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
